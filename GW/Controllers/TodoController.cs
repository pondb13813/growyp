﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GW.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GW.Models;
using Microsoft.EntityFrameworkCore;
using GW.Class;
using Microsoft.Extensions.Configuration;

namespace GW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {

        private readonly GU_Context _context;
        private ClassResource _CLSR;
        private IConfiguration _iconfiguration;

        public TodoController(GU_Context context,IConfiguration iconfiguration)
        {
            _context = context;
            _iconfiguration = iconfiguration;
        }

        // GET: api/Todo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ToDo_Task>>> GetTodoItems()
        {
            string user_id_string = HttpContext.Session.GetString("User_ID");
            int user_id;


            try
            {
                user_id = Convert.ToInt32(user_id_string);
            }
            catch
            {
                user_id = 0;

            }

            return await _context.ToDo_Task.Where(u=>u.UserInfo.User_ID == user_id).ToListAsync();
        }

        // GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ToDo_Task>> GetTodoItem(long id)
        {
            string user_id_string = HttpContext.Session.GetString("User_ID");
            int user_id;


            try
            {
                user_id = Convert.ToInt32(user_id_string);
            }
            catch
            {
                user_id = 0;

            }

            var todoItem = await _context.ToDo_Task.Where(i=>i.User_ID == user_id).SingleOrDefaultAsync();

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }



    }
}