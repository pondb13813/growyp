﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GW.Data;
using GW.Models;
using GW.Class;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;

namespace GW.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {

        private readonly GU_Context _context;
        private ClassResource _CLSR;
        //private IConfiguration _iconfiguration;

        public static IConfiguration Configuration;

        public ServicesController(GU_Context context, IConfiguration configuration)
        {
            _context = context;
            _CLSR = new ClassResource(_context, configuration);
            //HCON = new HomeController(_context, configuration);

            var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();


        }


        //[HttpGet("Users/LineNoti_Success")]
        [ActionName("LineNoti_Success")]

        [HttpGet]
        public IActionResult LineNoti_Success([FromQuery(Name = "code")]string code, [FromQuery(Name = "state")]string state)
        {

            //ใช้ State แทน Session

            if (code != null)
            {
                string codeLine = code;


                string result = "";
                var user = _context.User.Where(i => i.User_ID == Convert.ToInt32(state)).FirstOrDefault();


                //วิธีพี่ออฟ
                WebClient wc = new WebClient();
                string targetAddress = "https://notify-bot.line.me/oauth/token";
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                NameValueCollection nc = new NameValueCollection();
                nc["grant_type"] = "authorization_code";
                nc["code"] = code;
                nc["redirect_uri"] = Configuration["Config:redirect_uri"];
                nc["client_id"] = Configuration["Config:client_id"];
                nc["client_secret"] = "u3TNMTcrO9cyepUutJPxECkxC8hVvjYMg2XxCIKykq0";
                byte[] bResult = wc.UploadValues(targetAddress, nc);

                result = Encoding.UTF8.GetString(bResult);


                user.Line_Code_QueryString = code;

                var objResponse = JsonConvert.DeserializeObject<line_res>(result);

                user.Line_AccessToken = objResponse.access_token;

                _context.Update(user);
                _context.SaveChanges();


                HttpContext.Session.SetString("LineNoti", "Success");
                HttpContext.Session.SetString("User_ID", state);

                return RedirectToAction("Add_Task", "Todo_Task");


            }
            else
            {
                HttpContext.Session.SetString("LineNoti", "GET with no params.");

                return RedirectToAction("Add_Task", "Todo_Task");

            }

            //var user_id_string = HttpContext.Session.GetString("User_ID");
            //var user_id = 0;

            //try
            //{
            //    user_id = Convert.ToInt32(user_id_string);
            //}
            //catch
            //{
            //    user_id = 0;
            //}

            //if (user_id_string != null)
            //{

            //    if (code != null)
            //    {
            //        string codeLine = code;


            //        string result = "";
            //        var user = _context.User.Where(i => i.User_ID == user_id).SingleOrDefault();


            //        //วิธีพี่ออฟ
            //        WebClient wc = new WebClient();
            //        string targetAddress = "https://notify-bot.line.me/oauth/token";
            //        wc.Encoding = Encoding.UTF8;
            //        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            //        NameValueCollection nc = new NameValueCollection();
            //        nc["grant_type"] = "authorization_code";
            //        nc["code"] = code;
            //        nc["redirect_uri"] = Configuration["Config:redirect_uri"];
            //        nc["client_id"] = Configuration["Config:client_id"];
            //        nc["client_secret"] = "u3TNMTcrO9cyepUutJPxECkxC8hVvjYMg2XxCIKykq0";
            //        byte[] bResult = wc.UploadValues(targetAddress, nc);

            //        result = Encoding.UTF8.GetString(bResult);


            //        user.Line_Code_QueryString = code;

            //        var objResponse = JsonConvert.DeserializeObject<line_res>(result);

            //        user.Line_AccessToken = objResponse.access_token;

            //        _context.Update(user);
            //        _context.SaveChanges();


            //        HttpContext.Session.SetString("LineNoti", "Success");

            //        return RedirectToAction("Add_Task", "Todo_Task");


            //    }
            //    else
            //    {
            //        HttpContext.Session.SetString("LineNoti", "GET with no params.");

            //        return RedirectToAction("Add_Task", "Todo_Task");

            //    }
            //}
            //else
            //{
            //    HttpContext.Session.SetString("LineNoti", "You havn't login yet.");

            //    return RedirectToAction("Contact","Home");
            //}

        }

        public class line_res
        {
            public string status;
            public string message;
            public string access_token;
        }

    }
}