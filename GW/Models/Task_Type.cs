﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GW.Models
{
    public class Task_Type
    {

        [Key]
        public int task_Type_ID { get; set; }

        public string task_Type_Name { get; set; }

        public string task_Type_Status { get; set; }


    }
}
