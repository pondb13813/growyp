﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GW.Models
{
    public class Achievement_Type
    {
        [Key]
        public int AC_ID { get; set; }
        public string AC_Name { get; set; }
        public string AC_Description { get; set; }

        public string AC_Status { get; set; }


    }
}
