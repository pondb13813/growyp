﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GW.Models
{
    public class AlertModel
    {

        public string header { get; set; }
        public string message { get; set; }
        public string type { get; set; }
    }
}
