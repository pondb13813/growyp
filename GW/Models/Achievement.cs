﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GW.Models
{
    public class Achievement
    {
        [Key]

        public int AC_Running { get; set; }


        [ForeignKey("Achievement_Type")]
        public int AC_ID { get; set; }

        public string isComplete { get; set; }
        public string Completed_Date { get; set; }
        public int User_ID { get; set; }

        public Achievement_Type Achievement_Type { get; set; }
    }
}
