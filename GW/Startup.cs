﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using GW.Class;
using GW.Data;
using GW.Resources;
using GW.Services;
using GW.Services.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace GW
{
    public class Startup
    {
        public Startup(IConfiguration configuration, Microsoft.AspNetCore.Hosting.IHostingEnvironment _env)
        {
            Configuration = configuration;
            env = _env;
        }

        public Microsoft.AspNetCore.Hosting.IHostingEnvironment env { get; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //NEW ADD
            //var connection = @"User Id=admin;PASSWORD=Olympians13813sd;SERVER=203.151.47.84;database=GU_DB";
            //services.AddDbContext<GU_Context>
            //    (options => options.UseSqlServer(connection));

            services.AddDbContext<GU_Context>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //User Id=admin;PASSWORD=Olympians13813sd;SERVER=WIN-6PVIS4LUVTK;database=GU_DB

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession(options =>
            {
                Guid g = Guid.NewGuid();
                //ส่วนนี้เป็น Security 
                //options.Cookie.SecurePolicy = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                options.Cookie.Name = "SESSION_" + g.ToString();
                options.Cookie.HttpOnly = true;
                options.IdleTimeout = TimeSpan.FromMinutes(9999);
                options.IOTimeout = TimeSpan.FromMinutes(9999);
            });

            //Newly Add 19-04-19
            services.AddSingleton<LocService>();
            //services.AddSingleton<IHostedService, BG_Todo>();
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                              .AddViewLocalization()
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                    {
                        var assemblyName = new AssemblyName(typeof(SharedResource).GetTypeInfo().Assembly.FullName);
                        return factory.Create("SharedResource", assemblyName.Name);
                    };
                });

            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    var supportedCultures = new List<CultureInfo>
                        {
                            new CultureInfo("en-US"),
                            //new CultureInfo("de-CH"),
                            //new CultureInfo("fr-CH"),
                            new CultureInfo("th-TH")
                        };

                    options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;

                    // You can change which providers are configured to determine the culture for requests, or even add a custom
                    // provider with your own logic. The providers will be asked in order to provide a culture for each request,
                    // and the first to provide a non-null result that is in the configured supported cultures list will be used.
                    // By default, the following built-in providers are configured:
                    // - QueryStringRequestCultureProvider, sets culture via "culture" and "ui-culture" query string values, useful for testing
                    // - CookieRequestCultureProvider, sets culture via "ASPNET_CULTURE" cookie
                    // - AcceptLanguageHeaderRequestCultureProvider, sets culture via the "Accept-Language" request header
                    options.RequestCultureProviders.Insert(0, new QueryStringRequestCultureProvider());
                });

            //Newly Add
            services.AddDistributedMemoryCache();

            if (env.IsDevelopment())
            {
                services.AddSingleton<IScheduledTask, DueTimeMessageServices>();
                services.AddSingleton<IScheduledTask, SM_AlertService>();
                services.AddSingleton<IScheduledTask, CheckNoTask>();

                services.AddSingleton<IHostedService, SchedulerHostedService>();
                Console.WriteLine("dev mode");
            }
            else
            {

                services.AddSingleton<IScheduledTask, DueTimeMessageServices>();
                services.AddSingleton<IScheduledTask, SM_AlertService>();
                services.AddSingleton<IScheduledTask, CheckNoTask>();


                //services.AddSingleton<IScheduledTask, NewsScrapper>();
                services.AddSingleton<IHostedService, SchedulerHostedService>();

            }



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //Update 19-04-19
            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
