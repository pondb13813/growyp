﻿

function openLoginModal() {
    document.getElementById("modalOpen").click();
}

function alert_bt(txtHeader, txt, alertClass) {

    $('#alertTab').show();
    //Remove All Class
    $('#alertTab').removeClass("alert-success");
    $('#alertTab').removeClass("alert-info");
    $('#alertTab').removeClass("alert-warning");
    $('#alertTab').removeClass("alert-danger");
    $('#alertTab').removeClass("alert-primary");
    $('#alertTab').removeClass("alert-secondary");
    $('#alertTab').removeClass("alert-dark");
    $('#alertTab').removeClass("alert-light");
    $('#alertTab').removeClass("show");


    $('#alertTab').addClass("alert-" + alertClass);
    $('#alertTab').addClass("show");
    $('#alertText').empty();
    $('#alertText').append("<strong>" + txtHeader + "</strong> " + txt);

}





$(document).ready(function () {

 


    $('#modalOpen').hide();
    $('#msgLogin').hide();





    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {

        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });

    $(".dataTables_scrollHeadInner").css({ "width": "100%" });

    $(".table ").css({ "width": "100%" });

    $('.chosen-select').chosen(
    );

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Data not found !!' },
        '.chosen-select-rtl': { rtl: true },
        '.chosen-select-width': { width: '95%' },

    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }



    //Enter to Login
    var password_field = document.getElementById("input_Password");
    var email_field = document.getElementById("input_Email");

    password_field.addEventListener("keyup", function (event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Trigger the button element with a click
            document.getElementById("submit_login").click();
        }
    });

    email_field.addEventListener("keyup", function (event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Trigger the button element with a click
            document.getElementById("submit_login").click();
        }
    });


                //$('#alertModal').modal();

                //alertModal_Popup("HELLO", "danger","THIS TEXST");

            





});




    function alertModal_Popup(title, type_color, appendHTML) {
        $('#alertModal #alertModal_Title').text(title);


        var color = "";

        if (type_color == "danger") {
            color = "#DC3545";
            fontColor = "White";
        }
        else if (type_color == "success") {
            color = "#28A745";
            fontColor = "White";
        }
        else if (type_color == "warning") {
            color = "#FFC107";
            fontColor = "White";
        }
        else if (type_color == "secondary") {
            color = "#6C757D";
            fontColor = "White";
        }
        else if (type_color == "gu") {
            color = "#1C1408";
            fontColor = "White";
        }
        else {
            color = "#6C757D";
            fontColor = "White";
        }

        var $description = $('<div/>');
        $description.append($('<p/>').html(appendHTML));

        $('#alertModal #alertModal_Detail').empty().html($description);
        $('#alertModal_Header').css('background-color', color);
        $('#alertModal #alertModal_Title').css('color', fontColor);

        $('#alertModal').modal();
}