﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Configuration;
using GW.Models;

namespace GW.Data
{
    public class GU_Context : DbContext
    {

        public GU_Context(DbContextOptions<GU_Context> options)
            : base(options)
        { }


 

        public DbSet<User> User { get; set; }
        public DbSet<ToDo_Task> ToDo_Task { get; set; }
        public DbSet<Trees> Trees { get; set; }
        //public DbSet<UserMapping> UserMapping { get; set; }
        public DbSet<Tree_Type> Tree_Type { get; set; }
        public DbSet<Task_Type> Task_Type { get; set; }

        public DbSet<Achievement> Achievement { get; set; }
        public DbSet<Achievement_Type> Achievement_Type { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<ToDo_Task>().ToTable("ToDo_Task");
            modelBuilder.Entity<Trees>().ToTable("Trees");
            modelBuilder.Entity<Tree_Type>().ToTable("Tree_Type");
            modelBuilder.Entity<Task_Type>().ToTable("Task_Type");
            modelBuilder.Entity<Achievement>().ToTable("Achievement");
            modelBuilder.Entity<Achievement_Type>().ToTable("Achievement_Type");



            //modelBuilder.Entity<UserMapping>().ToTable("UserMapping").HasKey(c => new { c.User_ID, c.Tree_ID });


        }

    }
}
