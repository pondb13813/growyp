﻿using GW.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GW.Class
{
    public class BG_Todo : BackgroundService
    {
        private readonly ILogger<BG_Todo> _BG_TodoLogger;
        private readonly GU_Context _context;
        private ClassResource _CLSR;
        public static IConfiguration Configuration { get; set; }

        //private readonly IEmailService _emailService;
        public BG_Todo(ILogger<BG_Todo> BG_TodoLogger,
            GU_Context context, IConfiguration configuration)
        {
            _BG_TodoLogger = BG_TodoLogger;
            _context = context;
            Configuration = configuration;
            _CLSR = new ClassResource(context, configuration);
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _BG_TodoLogger.LogDebug("BG_Todo is starting");
            stoppingToken.Register(() => _BG_TodoLogger.LogDebug("BG_Todo is stopping."));
            while (!stoppingToken.IsCancellationRequested)
            {
                _BG_TodoLogger.LogDebug("BG_Todo is running in background");

                //var pendingEmailTasks = _demoContext.EmailTasks
                //    .Where(x => !x.IsEmailSent).AsEnumerable();

                //await SendEmailsAsync(pendingEmailTasks);

                String cDate = _CLSR.GetDateNow("");
                String cTime = _CLSR.GetTimeNow("");

                

                var nearlyFail_Todo = _context.ToDo_Task.Where(i => i.Task_Due_Date == cDate && (Convert.ToInt32(i.Task_Due_Time) - Convert.ToInt32(cTime)) <= 15).ToList();

                foreach(var item in nearlyFail_Todo)
                {
                    _CLSR.sendMessage(item.User_ID, "\"" + item.Task_Name + "\" ใกล้จะครบเวลาที่กำหนดแล้ว! " + "[" + _CLSR.InsertTimeToDisplay(item.Task_Due_Time,":") + "]");
                }

                await Task.Delay(1000 * 60 * 1, stoppingToken);
            }

            _BG_TodoLogger.LogDebug("BG_Todo is stopping");
        }

    }
}
