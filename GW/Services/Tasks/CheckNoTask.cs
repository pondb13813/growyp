﻿

using GW.Class;
using GW.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace GW.Services.Tasks
{
    public class CheckNoTask : IScheduledTask
    {

        //public string Schedule => "* * * * *"; Every 1 minutes
        public string Schedule => "59 23 * * *";

        private readonly IServiceScopeFactory _scopeFactory;




        public static IConfiguration Configuration { get; set; }

        //public ClassResource _CLRS;
        //private readonly GU_Context _context;

        public CheckNoTask(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            //_context = context;

            var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);


            Configuration = builder.Build();

            //_CLRS = new ClassResource(context, Configuration);
        }

        public String GetDateNow(String sSeperate = "")
        {
            string DOut;
            Int32 DYear;

            DYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            if (DYear > 2500)
            { DYear = DYear - 543; }

            DOut = Convert.ToString(DYear) + sSeperate + DateTime.Now.ToString("MM") + sSeperate + DateTime.Now.ToString("dd");

            return DOut;
        }




        public String GetTimeNow(String sSeperate = "")
        {
            string TOut;

            TOut = DateTime.Now.ToString("HH") + sSeperate + DateTime.Now.ToString("mm") + sSeperate + DateTime.Now.ToString("ss");

            return TOut;
        }


        public String ConvertDatePicker(String dateInput, String symbol)
        {

            var newDate = dateInput.Replace(symbol, String.Empty);


            var day = newDate.Substring(0, 2);
            var month = newDate.Substring(2, 2);
            var year = newDate.Substring(4, 4);

            int new_Year = Convert.ToInt32(year);

            try
            {
                if (Convert.ToInt32(year) >= 2400)
                {
                    new_Year = Convert.ToInt32(year) - 543;
                }

            }
            catch
            {

            }


            var convertedDate = new_Year.ToString() + month + day;

            return convertedDate;

        }


        public string ConvertDateDBtoSymbol(string dateInput, string symbol)
        {
            var day = dateInput.Substring(6, 2);
            var month = dateInput.Substring(4, 2);
            var year = dateInput.Substring(0, 4);


            return day + symbol + month + symbol + year;
        }

        public string InsertTimeToDisplay(string timeInput, string symbol)
        {
            var hour = timeInput.Substring(0, 2);
            var min = timeInput.Substring(2, 2);

            return hour + symbol + min;

        }


        //SEND LINE MESSAGE
        public bool sendMessage(int user_id, string msg)
        {

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();

                var user = dbContext.User.Where(u => u.User_ID == user_id).SingleOrDefault();
                var res = "";


                string result = "";

                try
                {

                    if (msg != null)
                    {
                        //วิธีพี่ออฟ
                        WebClient wc = new WebClient();
                        string targetAddress = "https://notify-api.line.me/api/notify";
                        wc.Encoding = Encoding.UTF8;
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + user.Line_AccessToken;
                        NameValueCollection nc = new NameValueCollection();
                        nc["message"] = msg;

                        byte[] bResult = wc.UploadValues(targetAddress, nc);

                        result = Encoding.UTF8.GetString(bResult);


                        return true;
                    }
                    else
                    {
                        //return Json("Message required");
                        return false;
                    }

                }
                catch (Exception e)
                {

                    return false;
                }


            }



        }

        public bool sendMessage(int user_id, string msg, string stickerPackageId, string stickerId)
        {

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();

                var user = dbContext.User.Where(u => u.User_ID == user_id).SingleOrDefault();
                var res = "";


                string result = "";

                try
                {

                    if (msg != null)
                    {
                        //วิธีพี่ออฟ
                        WebClient wc = new WebClient();
                        string targetAddress = "https://notify-api.line.me/api/notify";
                        wc.Encoding = Encoding.UTF8;
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + user.Line_AccessToken;
                        NameValueCollection nc = new NameValueCollection();
                        nc["message"] = msg;
                        nc["stickerPackageId"] = stickerPackageId;
                        nc["stickerId"] = stickerId;

                        byte[] bResult = wc.UploadValues(targetAddress, nc);

                        result = Encoding.UTF8.GetString(bResult);


                        return true;
                    }
                    else
                    {
                        //return Json("Message required");
                        return false;
                    }

                }
                catch (Exception e)
                {

                    return false;
                }


            }



        }


        public async void CheckDayIfNoTask()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();

                var hp_Down = 50;

                String cDate = GetDateNow("");
                String cTime = GetTimeNow("");

                String timeNow = cTime.Substring(0, 4);

                sendMessage(2, cDate + " - " + timeNow + " Background Task (CheckDayIfNoTask) is working..");

                try
                {

                    var users = dbContext.User.ToList();

                    foreach(var u in users)
                    {
                        var task_in_today = dbContext.ToDo_Task.Where(t => t.Task_Create_Date.Contains(cDate) && t.User_ID == u.User_ID).Count();
                        var user_Tree = dbContext.Trees.Where(i => i.User_ID == u.User_ID && i.Tree_Status == "S").SingleOrDefault();

                        if (task_in_today == 0)
                        {

                            //Level ลด และต้นไม้ค่อยๆตาย
                            user_Tree.Tree_HP = user_Tree.Tree_HP - hp_Down;

                            if (user_Tree.Tree_HP <= 0)
                            {
                                user_Tree.Tree_isDead = "Y";
                                user_Tree.Tree_HP = 0;



                                sendMessage(u.User_ID, "ต้นไม้ของคุณเหี่ยวเฉา เพราะไม่มีเป้าหมายสำหรับวันนี้!","2","526");



                            }
                            else
                            {
                                sendMessage(u.User_ID, "ต้นไม้ของคุณกำลังเฉา เพราะไม่มีเป้าหมายสำหรับวันนี้!");
                            }




                        }
                        else
                        {
                            // ส่ง Message ไปบอก ID ปอนด์ด้วย
                        }

                    }




                    


                }
                catch (Exception e)
                {

                    using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(@"bg_CheckNoTask_ErrorLog.txt", true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + " - Error: " + e.Message);
                    }

                }




            }
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"CheckNoTask.txt", true))
            {
                file.WriteLine(DateTime.Now.ToString());
            }

           
            CheckDayIfNoTask();


        }
    }
}
