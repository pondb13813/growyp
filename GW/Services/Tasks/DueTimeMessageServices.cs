﻿using GW.Class;
using GW.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GW.Services.Tasks
{
    public class DueTimeMessageServices : IScheduledTask
    {
        //public string Schedule => "* * * * *"; Every 1 minutes
        public string Schedule => "*/6 * * * *";

        private readonly IServiceScopeFactory _scopeFactory;




        public static IConfiguration Configuration { get; set; }

        //public ClassResource _CLRS;
        //private readonly GU_Context _context;

        public DueTimeMessageServices(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            //_context = context;

            var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);


            Configuration = builder.Build();

            //_CLRS = new ClassResource(context, Configuration);
        }


        public String GetDateNow(String sSeperate = "")
        {
            string DOut;
            Int32 DYear;

            DYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            if (DYear > 2500)
            { DYear = DYear - 543; }

            DOut = Convert.ToString(DYear) + sSeperate + DateTime.Now.ToString("MM") + sSeperate + DateTime.Now.ToString("dd");

            return DOut;
        }




        public String GetTimeNow(String sSeperate = "")
        {
            string TOut;

            TOut = DateTime.Now.ToString("HH") + sSeperate + DateTime.Now.ToString("mm") + sSeperate + DateTime.Now.ToString("ss");

            return TOut;
        }


        public String ConvertDatePicker(String dateInput, String symbol)
        {

            var newDate = dateInput.Replace(symbol, String.Empty);


            var day = newDate.Substring(0, 2);
            var month = newDate.Substring(2, 2);
            var year = newDate.Substring(4, 4);

            int new_Year = Convert.ToInt32(year);

            try
            {
                if (Convert.ToInt32(year) >= 2400)
                {
                    new_Year = Convert.ToInt32(year) - 543;
                }

            }
            catch
            {

            }


            var convertedDate = new_Year.ToString() + month + day;

            return convertedDate;

        }


        public string ConvertDateDBtoSymbol(string dateInput, string symbol)
        {
            var day = dateInput.Substring(6, 2);
            var month = dateInput.Substring(4, 2);
            var year = dateInput.Substring(0, 4);


            return day + symbol + month + symbol + year;
        }

        public string InsertTimeToDisplay(string timeInput, string symbol)
        {
            var hour = timeInput.Substring(0, 2);
            var min = timeInput.Substring(2, 2);

            return hour + symbol + min;

        }


        //SEND LINE MESSAGE
        public bool sendMessage(int user_id, string msg)
        {

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();

                var user = dbContext.User.Where(u => u.User_ID == user_id).SingleOrDefault();
                var res = "";


                string result = "";

                try
                {

                    if (msg != null)
                    {
                        //วิธีพี่ออฟ
                        WebClient wc = new WebClient();
                        string targetAddress = "https://notify-api.line.me/api/notify";
                        wc.Encoding = Encoding.UTF8;
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + user.Line_AccessToken;
                        NameValueCollection nc = new NameValueCollection();
                        nc["message"] = msg;

                        byte[] bResult = wc.UploadValues(targetAddress, nc);

                        result = Encoding.UTF8.GetString(bResult);


                        return true;
                    }
                    else
                    {
                        //return Json("Message required");
                        return false;
                    }

                }
                catch (Exception e)
                {

                    return false;
                }


            }
            


        }

        public bool sendMessage(int user_id, string msg, string stickerPackageId, string stickerId)
        {

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();

                var user = dbContext.User.Where(u => u.User_ID == user_id).SingleOrDefault();
                var res = "";


                string result = "";

                try
                {

                    if (msg != null)
                    {
                        //วิธีพี่ออฟ
                        WebClient wc = new WebClient();
                        string targetAddress = "https://notify-api.line.me/api/notify";
                        wc.Encoding = Encoding.UTF8;
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + user.Line_AccessToken;
                        NameValueCollection nc = new NameValueCollection();
                        nc["message"] = msg;
                        nc["stickerPackageId"] = stickerPackageId;
                        nc["stickerId"] = stickerId;

                        byte[] bResult = wc.UploadValues(targetAddress, nc);

                        result = Encoding.UTF8.GetString(bResult);


                        return true;
                    }
                    else
                    {
                        //return Json("Message required");
                        return false;
                    }

                }
                catch (Exception e)
                {

                    return false;
                }


            }



        }

        public void CheckTaskDueDate(int user_id, int hpDown)
        {

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();
                //var endpoint = dbContext.Primary_Master.Where(x => x.PrimaryCode.Equals(Configuration["Config:company"])).FirstOrDefault();


                String cDate = GetDateNow("");
                String cTime = GetTimeNow("");

                String TimeNow = cTime.Substring(0, 4);


                var user = dbContext.User.Where(i => i.User_ID == user_id).SingleOrDefault();
                var tree = dbContext.Trees.Where(i => i.User_ID == user_id && i.Tree_Status == "S").SingleOrDefault();

                //จะหาเจอแค่ Task ที่มีเงื่อนไขตามนี้ ถ้าเช็คครั้งต่อไปจะไม่ลบ ID ซ้ำๆ
                var task = dbContext.ToDo_Task.Where(i => i.User_ID == user_id && i.Task_isComplete == "N" && i.Task_isFail == "N" && i.Task_Status == "Y" && i.Task_Parent_ID == 0).ToList();




                //check task due date and time
                foreach (var item in task)
                {
                    //Task over due date.
                    if (Convert.ToInt32(cDate) > Convert.ToInt32(item.Task_Due_Date))
                    {
                        tree.Tree_HP = tree.Tree_HP - hpDown;

                        if (tree.Tree_HP <= 0)
                        {
                            tree.Tree_isDead = "Y";

                            sendMessage(user_id, "ต้นไม้ของคุณเหี่ยวเฉา เพราะเป้าหมายล้มเหลวมากเกินไป!");

                            //tree.Tree_Status = "N";

                        }


                        //Task inComplete
                        item.Task_isFail = "Y";
                        item.Task_isComplete = "N";


                        dbContext.Update(tree);
                        dbContext.Update(item);
                        dbContext.SaveChanges();



                        sendMessage(user_id, "เป้าหมาย \"" + item.Task_Name + "\" ล้มเหลว! เนื่องจากเกินกำหนดเวลา " + ConvertDateDBtoSymbol(item.Task_Due_Date, "/") + " " + InsertTimeToDisplay(item.Task_Due_Time, ":") + " น.");


                    }
                    //On Date but Time is up.
                    else if (Convert.ToInt32(item.Task_Due_Date) == Convert.ToInt32(cDate) && Convert.ToInt32(TimeNow) > Convert.ToInt32(item.Task_Due_Time))
                    {
                        tree.Tree_HP = tree.Tree_HP - hpDown;

                        if (tree.Tree_HP <= 0)
                        {
                            tree.Tree_Level = 0;
                            //tree.Tree_Status = "N";

                        }

                        //Task inComplete
                        item.Task_isFail = "Y";
                        item.Task_isComplete = "N";



                        dbContext.Update(tree);
                        dbContext.Update(item);
                        dbContext.SaveChanges();

                        sendMessage(user_id, "เป้าหมาย \"" + item.Task_Name + "\" ล้มเหลว! เนื่องจากเกินกำหนดเวลา " + ConvertDateDBtoSymbol(item.Task_Due_Date, "/") + " " + InsertTimeToDisplay(item.Task_Due_Time, ":") + " น.");

                    }
                    else
                    {

                    }


                }


            }

        }

        public async void CheckDueTime()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<GU_Context>();
                //var endpoint = dbContext.Primary_Master.Where(x => x.PrimaryCode.Equals(Configuration["Config:company"])).FirstOrDefault();

                String cDate = GetDateNow("");
                String cTime = GetTimeNow("");

                String timeNow = cTime.Substring(0, 4); 

                try
                {

                    //SEND MESSAGE TO MY LINE
                    sendMessage(2, cDate + " " + cTime + "= Background task is working. (CheckDueTime)");

                    //CheckDueDate and Time

                    var allUser = dbContext.User.ToList();

                    foreach(var user in allUser)
                    {

                        CheckTaskDueDate(user.User_ID,20);

                    }

                    //SEND Message to 15 min before fail!
                    var nearlyFail_Todo = dbContext.ToDo_Task.Where(i => i.Task_Due_Date == cDate && (Convert.ToInt32(i.Task_Due_Time) - Convert.ToInt32(timeNow)) <= 15 && i.Task_isComplete == "N" && i.Task_isFail == "N").ToList();

                    foreach (var item in nearlyFail_Todo)
                    {
                        sendMessage(item.User_ID, "\"" + item.Task_Name + "\" ใกล้จะครบเวลาที่กำหนดแล้ว! " + "[" + InsertTimeToDisplay(item.Task_Due_Time, ":") + "]","4","617");
                    }
                }
                catch (Exception e)
                {

                    using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(@"bgErrorLog.txt", true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + " - Error: " + e.Message);
                    }

                }

                


            }
        }


        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"write.txt", true))
            {
                file.WriteLine(DateTime.Now.ToString());
            }

            CheckDueTime();


        }

    }
}
